from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext,loader
from django.http import HttpResponse
from pymongo import MongoClient
from django.utils.safestring import mark_safe

import collections
import pymongo			
import MySQLdb
ip_address="10.105.24.33"
mongo_port=27017
sql_user="root"
sql_pswd="edx"
database="edxapp"
"""Description:Function to get grades of a student in each exam(quiz,mid term,final exam etc.).It returns a list of grsdes scored in all exams by a student
   Input Parameters:
       student_id :User_id of student
       course_id : id of course
   Output Type : List
   Author: Samridhi
   Date of creation: 17 june 2015
"""
def get_student_course_grades(student_id,course_id):
	results_list=[]		#A list to store the final results
	sub_results_list=[]		#A list which shall be embedded within the results_list list
	highchart_list=[]       #List to be returned for highcharts
        try:
                client=pymongo.MongoClient(ip_address,mongo_port) 	#Establishing MongoDB connection
        except:
                print "MongoDB connection not established"
                return HttpResponse("MongoDB connection not established")
        try:
                db_mysql=MySQLdb.connect(ip_address,sql_user,sql_pswd,database)
        except:
                print "MySQL connection not established"                #Establishing MySQL connection
                return HttpResponse("MySQL connection not established")
	db_mongo=client.edxapp		#Getting the object for edxapp database of MongoDB
	mongo_cur=db_mongo.modulestore.find({'_id.course':course_id,'_id.category':'course'},{'definition.data.grading_policy.GRADER.':1,'_id':0})		#Query to get the grading policy of a partticular course
	try:
                strquery="Select grade,max_grade from courseware_studentmodule where max_grade is not null and grade is not null and student_id=\'"+str(student_id)+"\' and module_id=\'"
		i=mongo_cur[0]
		stud_avg_tot=0			
		list1=i['definition']['data']['grading_policy']['GRADER']		#Getting the GRADER list which stores the different formats and their weights in a course
		for j in range(len(list1)):      				#iterating over the formats
			best_score_list=[]					#This list will store the final scores for the particular format
			drop_count=list1[j]['drop_count']			#Gives number of droppable sections for that problem
			type=list1[j]['type']				#Gives the type of the format i.e. Quiz, Final Exam etc.
			weight=list1[j]['weight']			#Gives the weights of the formats 
			min_count=list1[j]['min_count']	#Gives the minimum number of sections of that type present in the course
			sub_results_list=[]		#initializing the sub_results to an empty list
			mongo_cur2=db_mongo.modulestore.find({'_id.course':course_id,'_id.category':'sequential','metadata.format':type,'metadata.graded':True},{'metadata':1,'definition.children':1})            
			#Query to find the different sequentials having the format 'type' 
			sequential_coun=0		#intializing sequential count to zero
			for k in mongo_cur2:	
				sequential_coun+=1				
				avg_score_sequential=0		
				sum_avg_prob_score=0		
                                sum_prob_score_obt=0
                                sum_tot_prob_score=0
				coun_prob=0					#Initializing problem count as zero
				list2=k['definition']['children']		#Getting the children list of the sequential, this will consist of vertical ids
				for m in range(len(list2)):				#Iterating over the list of vertical ids
					child_id=list2[m]			#Getting the vertical id
					arr=child_id.split('/')							#Splitting the vertical id to get the _id.name field for the vertical
					vertical_id=arr[len(arr)-1]						#Getting the vertical's _id.name
					mongo_cur3=db_mongo.modulestore.find({'_id.name':vertical_id,'_id.course':course_id,'_id.category':'vertical'},{'definition.children':1}).limit(1)
					#query to get the vertical document with the _id.name as vertical id
					n=mongo_cur3[0]			
					list3=n['definition']['children']                       #getting the children array for this vertical, consisiting of list of component ids
					for o in range(len(list3)):                                     #Iterating over the list of component ids
						comp_id=list3[o]  #Getting the component id
						arr2=comp_id.split('/')                         #Splitting the component id to get the _id.name field for the problem
						component_id=arr2[len(arr2)-1]          #Getting the component's _id.name
						mongo_cur4=db_mongo.modulestore.find({'_id.name':component_id,'_id.course':course_id,'_id.category':'problem'},{'metadata.weight':1}).limit(1)
						#query to get the problem document with the _id.name as problem id and category as problem.
						try:
							p=mongo_cur4[0]
							problem_id=comp_id                              #Getting the module_id for that problem
							mysql_cur=db_mysql.cursor()                     #Getting MySQL cursor object
							#query="Select grade,max_grade from courseware_studentmodule where student_id=\'"+student_id+"\' and module_id=\'"+problem_id+"\' and max_grade is not null and grade is not null" 
                                                        query=strquery+problem_id+"\'"
							#Query to get the grades for the student for that particular problem
							mysql_cur.execute(query)                #Executing query
							row=mysql_cur.fetchone()                #Fetching the row returned, only one row shall be returned
							try:
								grade=row[0]                            #Getting the grade of the student for this problem
								maxgrade=row[1]                         #Getting the max_grade of the student for this problem
								try:
									weight_of_problem=p['metadata']['weight']                      #Getting the weight of the problem      
								except:
									weight_of_problem=maxgrade                              #If no weight is defined, weight=maxgrade
					
								score_obt=grade*weight_of_problem/maxgrade              #Weighted score obtained for this problem
								tot_score=weight_of_problem                             #Weighted total score for this problem
								sum_prob_score_obt+=score_obt
								sum_tot_prob_score+=tot_score
							except:
								try:
									weight_of_problem=p['metadata']['weight']
								except: 
									weight_of_problem=0             #If weight is not defined and the problem has not been attempted
								sum_tot_prob_score+=weight_of_problem
						except:
							continue
                                                                
		
                                if sum_tot_prob_score>0:
					avg_score_sequential=sum_prob_score_obt/sum_tot_prob_score		#Calculating avg score of this sequential
                                else:
                                        avg_score_sequential=0
				sub_results_list.append([sequential_coun,avg_score_sequential])			
				highchart_list.append([str(type),str(sequential_coun),str(avg_score_sequential)])
				best_score_list.append(avg_score_sequential)		#Adding the sequential score to best_score_list
			best_score_list.sort(reverse=True)  #Sorting the scores list for that format in descending order
			sum_score_format=0			#Initializing sum score of format to 0
			for q in range(sequential_coun-drop_count):		#Getting the sum of best scores in the format
				sum_score_format+=best_score_list[q]
			if sequential_coun-drop_count>0:
                                avg_score_format=sum_score_format/(sequential_coun-drop_count)		#Getting average score of the format      
                                sub_results_list.append(['Avg',avg_score_format])				#Appending values to list
                                results_list.append([type,sub_results_list])
                                stud_avg_tot+=avg_score_format*weight
                        else:
                                avg_score_format=0
					#Getting total student average score
                
                if len(results_list)>0:
                        sub_results_list=[]
                        sub_results_list.append([1,stud_avg_tot])			#Appending final results in list
                        results_list.append(['Total',sub_results_list])
                        highchart_list.append(['Total',str(""),str(stud_avg_tot)])
	except:
		result_list=[]
	client.close()
	db_mysql.close()

	return highchart_list
"""
   Description: Function to get average grade and maximum grade in all courses associated to a particular faculty.
   Input Parameters:
           user-id: id of the faculty.
   Output Type: List
   Author : Sneha
   Date of creation: 18 june 2015
"""

def getall_course_grades(user_id):
        try:
                client=pymongo.MongoClient(ip_address,mongo_port) 	#Establishing MongoDB connection
        except:
                print "MongoDB connection not established"
                return HttpResponse("MongoDB connection not established")
        
        try:
                db_sql=MySQLdb.connect(ip_address,sql_user,sql_pswd,database)        #Connecting SQL
        except:
                print "MySQL connection not established"
                return HttpResponse("MySQL connection not established")
        result_list=[]
        sql_cur=db_sql.cursor()
        query="select course_id,count(user_id) as no_of_students from student_courseenrollment where course_id IN (select course_id from student_courseaccessrole where user_id=\'"+user_id+"\') group by course_id"
        #Query to get course id and no of students enrolled in each course
        sql_cur.execute(query)
        result=sql_cur.fetchall()
        for row in result:              #Iterating over each course
                course_list=[]
                course_list=get_student_grade(row[0],db_sql,client)           #Getting total grades of all students in a particular course
                if len(course_list)>0:    
                        result_list.append([str(row[0]),str(row[1]),str(course_list[0]),str(course_list[1])])             #Appending final result list
        db_sql.close()
        client.close()
        return result_list
                
                
""" Description: Function to get average grades of all students in a particular course.This function is called by getall_course_grade().
    Input Parameters:
            course_id: id of course passed as a parameter by function getall_course_grades.
            db_sql: MySQL Database connection object
            client: MongoDB connection object
    Output Type : List
    Author: Sneha
    Date of creation:18 june 2015
"""

def get_student_grade(course_id,db_sql,client):
        sum_grade=0
        count_student=0
        grade_list=[]
        course_name=course_id.split('/')                #Splitting course id to get course name
        c_name=course_name[1]
        sql_cur=db_sql.cursor()
        query="select user_id,course_id from student_courseenrollment where course_id=\'"+course_id+"\'"
        #Query to get ids of students enrolled in a course
        sql_cur.execute(query)
        result=sql_cur.fetchall()
        for row in result:
                grade=get_student_course_grade2(row[0],c_name,db_sql,client)          #Getting grade of each student in each course
                if grade==-1:                                   #If grading policy is not defined in a course
                        return []
                grade_list.append(grade)                                #Appending students grade to list
                sum_grade+=grade                        #Adding grades of all students
                count_student+=1                       #Counting no of students
        if count_student>0:
                avg_grade=sum_grade/count_student              #Finding average grade obtained in a course
        grade_list.sort(reverse=True)                   #Sorting students grade
        max_grade=grade_list[0]                 #Getting maximum grade in a course
        main_list=[]                            #Appending final result list
        main_list.append(avg_grade)
        main_list.append(max_grade)
        return main_list
        
""" Description: Function to get average grade of each student in a paricular course.This function is called by functions get_student_grade() and get_course_grade().
    Input Parameters:
            student_id: user_id of a student
            course_id: id of course for which grades are to be calculated
            db_mysql: MySQL Database connection object
            client: MongoDB connection object
    Output: Student total average
    Author: Samridhi
    Date of creation: 17 june 2015    
"""              
    

	
def get_student_course_grade2(student_id,course_id,db_mysql,client):
	
	db_mongo=client.edxapp		#Getting the object for edxapp database of Mongo
	try:
                mongo_cur=db_mongo.modulestore.find({'_id.course':course_id,'_id.category':'course'},{'definition.data.grading_policy.GRADER.':1,'_id':0}) #Query to get the grading policy of a partticular course
		
		try:
                        strquery="Select grade,max_grade from courseware_studentmodule where max_grade is not null and grade is not null and student_id=\'"+str(student_id)+"\' and module_id=\'"    
			i=mongo_cur[0]
                        stud_avg_tot=0			#initializing the student total marks for the course as zero
                        list1=i['definition']['data']['grading_policy']['GRADER']		#Getting the GRADER list which stores the different formats and their weights in a course
                        for j in range(len(list1)):				#iterating over the formats
                                best_score_list=[]					#This list will store the final scores for the particular format
                                drop_count=list1[j]['drop_count']			#Gives number of droppable sections for that problem
                                type=list1[j]['type']				#Gives the type of the format i.e. Quiz, Final Exam etc.
                                weight=list1[j]['weight']			#Gives the weights of the formats 
                                min_count=list1[j]['min_count']	#Gives the minimum number of sections of that type present in the course
                                try:
                                        mongo_cur2=db_mongo.modulestore.find({'_id.course':course_id,'_id.category':'sequential','metadata.format':type,'metadata.graded':True},{'metadata':1,'definition.children':1})
                                        # Getting the sequentials of a  particular format
                                        sequential_coun=0		#intializing sequential count to zero
                                        for k in mongo_cur2:	#Iterating over the sequentials of format 'type'
                                                sequential_coun+=1		#Incrementing sequential count		
                                                avg_score_sequential=0		#Initializing average score of sequential as zero
                                                sum_avg_prob_score=0		#Initializing sum of average problem scores as zero
                                                sum_prob_score_obt=0
                                                sum_tot_prob_score=0
                                                coun_prob=0					#Initializing problem count as zero
                                                list2=k['definition']['children']		#Getting the children list of the sequential, this will consist of vertical ids
                                                for m in range(len(list2)):				#Iterating over the list of vertical ids
                                                        child_id=list2[m]			#Getting the vertical id
                                                        arr=child_id.split('/')							#Splitting the vertical id to get the _id.name field for the vertical
                                                        vertical_id=arr[len(arr)-1]						#Getting vertical's _id.name 
                                                        mongo_cur3=db_mongo.modulestore.find({'_id.name':vertical_id,'_id.course':course_id,'_id.category':'vertical'},{'definition.children':1}).limit(1)
                                                        #query to get the vertical document with the _id.name as vertical id
							try:
								n=mongo_cur3[0]
                                                                list3=n['definition']['children']			#getting the children array for this vertical, consisiting of list of component ids
                                                                for o in range(len(list3)):					#Iterating over the list of component ids
                                                                        comp_id=list3[o]	#Getting the component id
                                                                        arr2=comp_id.split('/')				#Splitting the component id to get the _id.name field for the problem
                                                                        component_id=arr2[len(arr2)-1]		#Getting component's _id.name
                                                                        mongo_cur4=db_mongo.modulestore.find({'_id.name':component_id,'_id.course':course_id,'_id.category':'problem'},{'metadata.weight':1}).limit(1)
                                                                        #query to get the problem document with the _id.name as problem id and category as problem.
									try:
										p=mongo_cur4[0]
										
                                                                                problem_id=comp_id				#Getting the module_id for that problem
                                                                                mysql_cur=db_mysql.cursor()			#Getting MySQL cursor object
                                                                                #query="Select grade,max_grade from courseware_studentmodule where student_id=\'"+str(student_id)+"\' and module_id=\'"+problem_id+"\' and max_grade is not null and grade is not null" 
                                                                                query=strquery+problem_id+"\'"
                                                                                #Query to get the grades for the student for that particular problem 
                                                                                try:
                                                                                        mysql_cur.execute(query)		#Executing query
                                                                                except:
                                                                                        f=0
                                                                                row=mysql_cur.fetchone()		#Fetching the row returned, only one row shall be returned
                                                                                try:
                                                                                        grade=row[0]				#Getting the grade of the student for this problem
                                                                                        maxgrade=row[1]				#Getting the max_grade of the student for this problem
                                                                                        try:
                                                                                                weight_of_problem=p['metadata']['weight']			#Getting the weight of the problem	
                                                                                        except:
                                                                                                weight_of_problem=maxgrade				#If no weight is defined, weight=maxgrade
                                                                                        score_obt=grade*weight_of_problem/maxgrade		#Weighted score obtained for this problem
                                                                                        tot_score=weight_of_problem				#Weighted total score for this problem
                                                                                        sum_prob_score_obt+=score_obt
                                                                                        sum_tot_prob_score+=tot_score    
                                                                                except:
                                                                        
                                                                                        try:
                                                                                                weight_of_problem=p['metadata']['weight']
                                                                                        except:
                                                                                                weight_of_problem=0          #if no weight is defined and the problem has not been attempted
                                                                                        sum_tot_prob_score+=weight_of_problem
                                                                        except:
										continue                 
							except:
								continue
                                                if sum_tot_prob_score>0:
                                                        avg_score_sequential=sum_prob_score_obt/sum_tot_prob_score		#Calculating avg score of this sequential
                                                best_score_list.append(avg_score_sequential)		#Adding the sequential score to best_score_list
                                except:
                                        
                                        return -1
                                best_score_list.sort(reverse=True)  #Sorting the scores list for that format in descending order
                                sum_score_format=0			#Initializing sum score of format to 0
                                for q in range(sequential_coun-drop_count):		#Getting the sum of best scores in the format
                                        sum_score_format+=best_score_list[q]
                                if sequential_coun-drop_count > 0 :
                                        avg_score_format=sum_score_format/(sequential_coun-drop_count)		#Getting average score of the format
                                        stud_avg_tot+=avg_score_format*weight		#Getting total student average score   
                                else:
                                        avg_score_format=0
        	except:
                        
			return -1
        except:
                
                return -1
        
        return stud_avg_tot

""" Description: Function to get average grade of all students enrolled in a course.
    Input Parameters:
            course_name: name of the course for which grades are to be calculated.
    Output Type : List
    Author: Samridhi
    Date of creation:18 june 2015
"""
	
	
def get_course_grades(course_name):
        try:
                client=pymongo.MongoClient(ip_address,mongo_port) 	#Establishing MongoDB connection
        except:
                print "MongoDB connection not established"
                return HttpResponse("MongoDB connection not established")
        try:
                db_mysql=MySQLdb.connect(ip_address,sql_user,sql_pswd,database)		#Establishing MySQL connection
        except:
                print "MySQL connection not established"
                return HttpResponse("MySQL connection not established")
        results=[]
	query2="select a.id, a.username, b.course_id from auth_user as a, student_courseenrollment as b where a.id=b.user_id"        #Getting all students and their courses
	mysql_cur2=db_mysql.cursor()
	mysql_cur2.execute(query2)
	y=mysql_cur2.fetchall()
	for row in y:                                   
                str2=row[2]
                arr=str2.split('/')
                course_id=arr[1]
                if course_id==course_name:
                        stud_name=row[1]
                        stud_id=row[0]                        #Appending list
                        stud_grade=get_student_course_grade2(str(row[0]),course_name,db_mysql,client)   #Getting score of each student
                        #if stud_grade==-1:
                                #stud_grade=0
                        
                        results.append([stud_name,stud_grade,stud_id])
	db_mysql.close()
	client.close()
	return results

""" Description: Function to get average grade of a student in all the courses enrolled along with the course maximum and average grade.
    Input Parameters:
            student_id: id of the student.
    Output Type : List
    Author: Sneha
    Date of creation:21 june 2015
"""

def get_student_allcourses_grade(request,student_id):
        result_list=[]
        try:
                db_sql=MySQLdb.connect(ip_address,sql_user,sql_pswd,database)  #establishing sql connection
        except:
                print "MySQL connection not established"
                return HttpResponse("MySQL connection not established")
        try:
                client=pymongo.MongoClient(ip_address,mongo_port)     #establishing mongodb connection
        except:
                print "MongoDB connection not established"
                return HttpResponse("MongoDB connection not established")
        sql_cur=db_sql.cursor()
        query="select course_id from student_courseenrollment where user_id=\'"+student_id+"\'"     #get all courses in which the student is enrolled
        try:
                sql_cur.execute(query)
        except:
                print "Cannot execute the query"
                db_sql.close()
                client.close()
                return HttpResponse("Cannot fetch data from database")
        result=sql_cur.fetchall()
        for row in result:
                course_avg_list=get_student_grade2(row[0],student_id,db_sql,client)
                c_id=row[0].split('/')
                course_name=c_id[1]
                if len(course_avg_list)>0:
                        student_grade=course_avg_list[2]
                        result_list.append([row[0],student_grade,course_avg_list[0],course_avg_list[1]])
                        
        db_sql.close()
        client.close()
        return render_to_response('newindex4.html',{'data':result_list})

""" Description: Function to get average grades of all students in a particular course.This function is called by get_student_allcourses_grade().
    Input Parameters:
            course_id: id of course passed as a parameter by function getall_course_grades.
            student_id: id of student 
            db_sql: MySQL Database connection object
            client: MongoDB connection object
    Output Type : List
    Author: Sneha
    Date of creation:21 june 2015
"""

def get_student_grade2(course_id,student_id,db_sql,client):
        sum_grade=0
        count_student=0
        grade_list=[]
        course_name=course_id.split('/')                #Splitting course id to get course name
        c_name=course_name[1]
        sql_cur=db_sql.cursor()
        student_grade=0
        query="select user_id,course_id from student_courseenrollment where course_id=\'"+course_id+"\'"   #Query to get ids of students enrolled in a course
        sql_cur.execute(query)
        result=sql_cur.fetchall()
        for row in result:
                grade=get_student_course_grade2(row[0],c_name,db_sql,client)          #Getting grade of each student in each course
                if grade==-1:                                   #If grading policy is not defined in a course
                        return []
                if student_id==str(row[0]):
                        student_grade=grade
                grade_list.append(grade)                                #Appending students grade to list
                sum_grade+=grade                        #Adding grades of all students
                count_student+=1                       #Counting no of students
        avg_grade=sum_grade/count_student              #Finding average grade obtained in a course
        grade_list.sort(reverse=True)                   #Sorting students grade
        max_grade=grade_list[0]                 #Getting maximum grade in a course
        main_list=[]                            #Appending final result list
        main_list.append(avg_grade)
        main_list.append(max_grade)
        main_list.append(student_grade)
        return main_list
               
                                  
                                  
                                  
                
                
               
        
	

